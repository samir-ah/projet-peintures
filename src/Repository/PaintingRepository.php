<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Painting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Painting|null find($id, $lockMode = null, $lockVersion = null)
 * @method Painting|null findOneBy(array $criteria, array $orderBy = null)
 * @method Painting[]    findAll()
 * @method Painting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaintingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Painting::class);
    }

    /**
     * @return Painting[] Returns an array of Painting objects
     * @var $max Number of last paintings to get
     */

    public function getLastPaintingList($max): array
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Painting[] Returns an array of Painting objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

//    /**
//     * @throws NonUniqueResultException
//     */
//    public function findOneBySomeField($value): ?Painting
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult();
//    }
    /**
     * @return Painting[] Returns an array of Painting objects
     */
    public function findAllPortfolio(Category $category): array
    {
        return $this->createQueryBuilder('p')
            ->andWhere(':category MEMBER OF p.category')
            ->andWhere('p.portfolio = TRUE')
            ->setParameter('category',$category)
            ->getQuery()
            ->getResult()
            ;
    }

}
